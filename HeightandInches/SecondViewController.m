//
//  SecondViewController.m
//  HeightandInches
//
//  Created by user on 10/25/17.
//  Copyright © 2017 cop2654.mdc.edu. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblHeight;

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    double newFeet = [self.feet doubleValue];
    double newInches = [self.inches doubleValue];
    double totalInches;
    totalInches = ((newFeet * 12)+newInches)*.0254;
    self.lblName.text = [NSString stringWithFormat:@"Hello %@ %@",self.fname, self.lname];
    self.lblHeight.text = [NSString stringWithFormat:@"Your height of %.0f feet and %.0f inches is %.02f meters", newFeet, newInches, totalInches];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
