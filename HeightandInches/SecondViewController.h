//
//  SecondViewController.h
//  HeightandInches
//
//  Created by user on 10/25/17.
//  Copyright © 2017 cop2654.mdc.edu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController
@property (nonatomic, strong)NSString* fname;
@property (nonatomic, strong)NSString* lname;
@property (nonatomic, strong)NSString* feet;
@property (nonatomic, strong)NSString* inches;

@end
