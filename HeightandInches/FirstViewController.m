//
//  ViewController.m
//  HeightandInches
//
//  Created by user on 10/25/17.
//  Copyright © 2017 cop2654.mdc.edu. All rights reserved.
//

#import "FirstViewController.h"
#import "SecondViewController.h"

@interface FirstViewController ()
@property (weak, nonatomic) IBOutlet UITextField *txtFname;
@property (weak, nonatomic) IBOutlet UITextField *txtLname;
@property (weak, nonatomic) IBOutlet UITextField *txtFeet;
@property (weak, nonatomic) IBOutlet UITextField *txtInches;

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    SecondViewController* destination = [segue destinationViewController];
    destination.fname = self.txtFname.text;
    destination.lname = self.txtLname.text;
    destination.feet = self.txtFeet.text;
    destination.inches = self.txtInches.text;
}
- (IBAction)txtFnameBegin:(id)sender {
        [self.txtFname becomeFirstResponder];
}
- (IBAction)txtFnameEnd:(id)sender {
    [self.view resignFirstResponder];
}
- (IBAction)txtLnameStart:(id)sender {
    [self.txtLname becomeFirstResponder];
}
- (IBAction)txtLnameEnd:(id)sender {
     [self.view resignFirstResponder];
}
- (IBAction)txtFeetStart:(id)sender {
     [self.txtFeet becomeFirstResponder];
}
- (IBAction)txtFeetEnd:(id)sender {
     [self.view resignFirstResponder];
}
- (IBAction)txtIncesStart:(id)sender {
    [self.txtInches becomeFirstResponder];
}
- (IBAction)txtInchesEnd:(id)sender {
     [self.view resignFirstResponder];
}


-(IBAction)returnFromSceneTwo:(UIStoryboardSegue *)unwindSegue{
    self.txtInches.text = NULL;
    self.txtFeet.text = NULL;
    self.txtFname.text = NULL;
    self.txtLname.text = NULL;
}


@end
