//
//  main.m
//  HeightandInches
//
//  Created by user on 10/25/17.
//  Copyright © 2017 cop2654.mdc.edu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
